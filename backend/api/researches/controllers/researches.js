'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */

module.exports = {
    byOrgId: async (ctx) => {
      try {
        const orgId = ctx.params.id;
        
        const res = await strapi.query('researches').model.query(qb => {
          qb.leftJoin ('researches__participant_orgs', 'research_id', 'researches.id')
        }).where({organization_id: orgId}).fetchAll();
        
        ctx.body = res.toJSON().map(r => {
            r.samples = r.samples.filter(s => +s.organization === +orgId);
            return r;
        });
        
      } catch (err) {
        ctx.body = err;
      }
    }
};
