import React, { useState } from 'react';
import { Form, Input, Button, Spin, Alert } from 'antd/es';
import { login } from '../services/api/login';
import { UserLoginResponse } from '../services/entities/user';

export interface LoginData {
    username: string,
    password: string
}

export interface LoginFormProps {
    onLogin?: (values: UserLoginResponse) => void
}

export default (props: LoginFormProps) => {
    
    const [errMessage, setErrMessage ] = useState<string>();
    const [isLoading, setLoading ] = useState<boolean>(false);
    
    const onFinish = (values: any) => {
        const loginData = values;
        setLoading(true);
        setErrMessage(undefined);
        login({identifier: loginData.username, password: loginData.password})
            .then (user => {
                props.onLogin && props.onLogin(user);
                setLoading(false);
            })
            .catch(() => {
                setErrMessage("Неверный логин или пароль")
                setLoading(false);
            })
    }
    return (
        <Spin spinning={isLoading}>
            <Form
                onFinish={onFinish}
                layout="vertical"
                name="login-form"
                >
                <Form.Item
                    label="Логин"
                    name="username"
                    rules={[{ required: true, message: 'Введите логин' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Пароль"
                    name="password"
                    rules={[{ required: true, message: 'Введите пароль' }]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item>
                    <Button size="large" type="primary" htmlType="submit">
                    Войти
                    </Button>
                </Form.Item>
                {errMessage && <Alert message={errMessage} type="error" showIcon />}
            </Form>
        </Spin>
    )
}
