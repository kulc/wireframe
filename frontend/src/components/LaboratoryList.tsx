import React, { useState, useEffect } from 'react';
import { getOrganizations } from '../services/api/organizations';
import { getResearches } from '../services/api/researches';
import { OrganizationTypes, Organization } from '../services/entities/organization';
import { List } from 'antd/es';
import { Link } from 'react-router-dom';
import { Research } from '../services/entities/research';

export const LaboratoryListComponent = () => {
    const [labs, setLabs] = useState<Organization[]>([]);
    const [researches, setResearches] = useState<Research[]>([]);
    useEffect(() => {
        getOrganizations({Type: OrganizationTypes.Laboratory}).then(labs => setLabs(labs) );
        getResearches().then(rs => setResearches(rs));
    },[]);
    
    return (
        <>
            <h2>Лаборатории</h2>
            <List
                dataSource={labs}
                renderItem={lab => 
                    <List.Item>
                        <List.Item.Meta 
                            title={lab.Name} 
                            description={<Link to={"/laboratory/" + lab.id}>Перейти к исследованиям</Link>}
                        />
                    </List.Item>}
            />
            <h2>Исследования</h2>
            <List
                dataSource={researches}
                renderItem={res => 
                    <List.Item>
                        <List.Item.Meta 
                            title={res.Title}
                            description={
                                <>
                                    <div>Лаборатории: {res.participantOrgs.map(org => (<span key={org.id}><Link to={"/laboratory/" + org.id}>{org.Name}</Link>; </span>))} </div>
                                    <div>Образцы: {res.samples.map(sample => <span key={sample.id}>{sample.name}; </span>)}</div>
                                    <div><Link to={"/researches/"+res.id+"/reports"}>Отчеты</Link></div>
                                </>
                            }
                        />
                    </List.Item>}
            />
        </>
    );
}