import React, { useState, useEffect } from 'react';
import { Samples } from '../services/entities/samples';
import { getSamplesByOrgId } from '../services/api/samples';
import { List, Alert, Spin } from 'antd/es';

export interface SamplesViewProps {
    orgId?: number
}
export const SamplesView = (props: SamplesViewProps) => {
    const [samples, setSamples] = useState<Samples[]>([]);
    const [isLoading, setIsloading] = useState<boolean>(false);
    useEffect( () =>{
        setIsloading(true);
        getSamplesByOrgId(props.orgId).then(samples => {
            setSamples(samples);
        })
        .finally(()=>setIsloading(false));
    }, [props.orgId]);

    return (<Spin spinning={isLoading}>
        {samples.length > 0 ? 
        <List>
            {samples.map(sample => <List.Item key={sample.id}>{sample.name}</List.Item>)}
        </List> : <Alert message="Образцы не найдены" type="warning" showIcon/>}
    </Spin>)
}