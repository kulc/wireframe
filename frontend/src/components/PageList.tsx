import React from 'react';
import { Page } from '../services/api/models';
import { Card } from 'antd/es';
import ReactMarkdown from 'react-markdown';

export interface PageListProps {
    pages: Page[]
}

export default (props: PageListProps) => {
        const pages = props.pages;
        return (
            <div>
                {pages.map((page)=><Card title={page.Name} key={"card_"+page.id}><ReactMarkdown source={page.Body} /></Card>)}
            </div>
        )
    
}
