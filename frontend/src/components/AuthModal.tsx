import React, { useContext, } from 'react';
import { Modal } from 'antd/es';
import LoginForm from './LoginForm';
import { UserContext } from '../services/contexts/userContext';
import { UserLoginResponse } from '../services/entities/user';
import { checkAuth } from '../services/api/login';
import { USER_TOKEN_NAME } from '../constants/common';

export interface AuthModalProps {
    showLoginModal: boolean,
    onCancel: () => void
}

export default (props: AuthModalProps) => {
    const userContextData = useContext(UserContext);

    const handleLogin = (user: UserLoginResponse) => {
        localStorage.setItem(USER_TOKEN_NAME, user.jwt);
        setUser();
    }

    const setUser = async () => {
        try {
            const userInfo = await checkAuth();
            userContextData?.login && userContextData.login(userInfo);
        } catch (error) {
            userContextData?.logout && userContextData.logout();
            localStorage.removeItem(USER_TOKEN_NAME);
        }
    }

    return (
        <Modal
            destroyOnClose
            closable={false}
            title="Авторизация"
            visible={props.showLoginModal}
            onCancel={()=>props.onCancel()}
            footer={""}
        >
            <LoginForm onLogin={handleLogin} />
        </Modal>
    )
}
