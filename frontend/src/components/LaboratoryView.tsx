import React, { useEffect, useState } from 'react';
import { getOrganization } from '../services/api/organizations';
import { Organization } from '../services/entities/organization';
import { Spin, Alert } from 'antd/es';

export interface LaboratoryProps {
    id: number
}

export const LaboratoryView = (props: LaboratoryProps) => {
    const [organization, setOrganization] = useState<Organization|undefined>();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    useEffect(()=>{
        setIsLoading(true);
        loadOrganization(props.id);
    }, [props.id]);

    const loadOrganization = (id: number) => {
        getOrganization(id).then(org => {
            setOrganization(org);
        })
        .catch(err => setOrganization(undefined))
        .finally(() => setIsLoading(false));
    }
    return (
        <Spin tip="Загрузка данных" spinning={isLoading}>
            {organization? 
                <h1>{organization?.Name}</h1>
                : !isLoading &&
                <Alert message="Организация не найдена" type="warning" showIcon />
            }
        </Spin>
    )
}