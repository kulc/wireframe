import React, { useState, useEffect } from 'react';
import { Research } from '../services/entities/research';
import { getResearchesByOrgId } from '../services/api/researches';
import { Alert, Spin, Tabs, Descriptions } from 'antd/es';
import ResearchTable from './ResearchTable';
import { getResultsByResearchAndOrg, getResults, updateResult, createResult } from '../services/api/research-results';
import { ResearchResult, NewResearchResult } from '../services/entities/research-results';

export interface ResearchViewProps {
    orgId?: number
}

interface ResearchWithResults extends Research {
    results?: ResearchResult[]
}

export const ResearchView = (props: ResearchViewProps) => {
    const [researches, setResearches] = useState<ResearchWithResults[]>([]);
    const [isLoading, setIsloading] = useState<boolean>(false);
    const { orgId } = props;
    useEffect(() => {
        initResearches(orgId);
    }, [orgId]);

    const initResearches = (orgId?: number) => {
        setIsloading(true);
        if (orgId){
            getResearchesByOrgId(orgId)
                .then(async (rs) => {
                    const researchesWithResults:ResearchWithResults[] = rs;
                    await Promise.all(researchesWithResults.map (async r => {
                        r.results = await getResultsByResearchAndOrg(r.id, orgId);
                    }));
                    setResearches(researchesWithResults)
                })
                .finally(()=>setIsloading(false));
        }
    }

    const onResultChange = async (result: NewResearchResult) => {
        setIsloading(true);
        getResults({
            organization: result.organization,
            research: result.research,
            sample: result.sample,
            testNumber: result.testNumber,
            types_of_test: result.types_of_test
        }).then(res => {
            if (res[0] && res[0].id){
                if (res[0].measurementResult !== result.measurementResult)
                    updateResult(res[0].id, result);
            }
            else{
                if (result.measurementResult !== null)
                    createResult(result);
            }
        }).finally(()=> { setIsloading(false); initResearches(orgId)} );
        
    }

    return (<Spin tip="Загрузка" spinning={isLoading}>
        {researches.length > 0 ? 
        <Tabs defaultActiveKey="1" tabPosition="left">
            {researches.sort( (a,b) => a.id - b.id ).map(research => 
                <Tabs.TabPane key={""+research.id} tab={research.Title}>
                    { (research.samples?.length  && research.samples.length > 0 && research.types_of_tests) ? 
                        <>
                            <Descriptions bordered size="small" column={1} style={{maxWidth: "300px"}}>
                                <Descriptions.Item label="Количество лабораторий">{research.numberOfLaboratory}</Descriptions.Item>
                                <Descriptions.Item label="Количество образцов">{research.numberOfSamples}</Descriptions.Item>
                                <Descriptions.Item label="Количество испытаний образца">{research.numberOfTests}</Descriptions.Item>
                            </Descriptions>
                            <h2>Результаты</h2>
                            <ResearchTable onResultChange={(resp) => {orgId && onResultChange({...resp, research: research.id, organization: orgId})}} samples={research.samples} typesOfTests={research.types_of_tests} numberOfTests={research.numberOfTests} results={research.results} />
                        </>
                        :
                        <Alert message="Для исследования должны быть определены образцы и виды исследований" type="error" showIcon/>
                    }
                </Tabs.TabPane>
            )}
        </Tabs> : !isLoading && <Alert message="Исследования не найдены" type="warning" showIcon/>}
    </Spin>)
}