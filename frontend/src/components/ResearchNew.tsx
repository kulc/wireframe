import React, { useContext } from 'react';
import NumberOfTests from './NumberOfTests';
import { UserContext } from '../services/contexts/userContext';
import { Roles } from '../services/entities/user';
import { Alert } from 'antd/es';

export const ResearchNew = () => {
    const userContextData = useContext(UserContext);
    if (userContextData?.user?.role.type !== Roles.Coordinate)
        return <Alert message="Требуется авторизация или нет доступа" type="error" showIcon />
    
    return (
    <>
        <h1>Новое исследование</h1>
        <NumberOfTests />
    </>)
}
