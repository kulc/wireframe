import React from 'react';
import { TypesOfTests } from '../services/entities/typesOfTests';
import { Table, Descriptions, Space, Row, Col } from 'antd/es';
import { Organization } from '../services/entities/organization';
import { Bar } from 'react-chartjs-2';

export interface ReportViewProps {
    reportByTestTypes: ReportByTestType[]
}

export interface ReportByTestType {
    typeOfTest: TypesOfTests,
    x: number[],
    p: number,
    xValue: number,
    s: number,
    сhr: number,
    absXMuinusXValue: number[],
    reportIterations: ReportIteration[],
    uX?: number,
    zindex?: {org: Organization, zindex: number}[]
}

export interface ReportIteration {
    xValueMinusChr: number,
    xValuePlusChr: number,
    xI: number[],
    xValueDivideP: number[],
    xValue: number,
    pMinusOne: number,
    arrXiMinXValueSquared: number[],
    sValue: number,
    chr: number
}

export const ReportView = (props: ReportViewProps) => {
    const { reportByTestTypes } = props;
    const columnsCount = 4;
    const fxd = 4;

    const getNumberTable = (values: number[]) => {
        let clmns:any[] = [];
        let data:any[] = [];
        values.forEach ( (v, idx) => {
            clmns.push( {dataIndex: 'k'+idx, title: idx.toString(), key: 'k'+idx} );
            data[0] =  {...data[0], ['k'+idx]: v.toFixed(fxd), key: idx};
        });
        return <Table showHeader={false} tableLayout="fixed" pagination={false} dataSource={data} columns={clmns} size="small"/>
    }

    return (<>
        <h2>Отчет по видам испытаний</h2>
        
        {reportByTestTypes.map( r => {
            const barData:any = {};
            barData.datasets = [];
            barData.labels = [];
            barData.datasets.push({label: "Средние по лабораториям", data: []});
            return (<div key={r.typeOfTest.id}>
                <h3>{r.typeOfTest.Name}</h3>
                    <Row>
                        <Col xs={24} sm={24} md={24} lg={24} xl={12}>
                            <Descriptions bordered layout="horizontal" size="small" column={1} style={{maxWidth: "400px", marginBottom: "16px"}}>
                                {r.zindex?.map(z => {
                                    barData.labels.push(z.org.Name)
                                    barData.datasets[0].data.push(z.zindex.toFixed(fxd));
                                    return <Descriptions.Item key={z.org.id} label={z.org.Name}>{z.zindex.toFixed(fxd)}</Descriptions.Item>
                                })}
                                <Descriptions.Item label="Ux" >{r.uX?.toFixed(fxd)}</Descriptions.Item>
                            </Descriptions>
                            <Bar data={barData} legend={{position: "bottom"}} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={24} sm={24} md={24} lg={24} xl={18}>
                            <Space direction="vertical">
                                <Descriptions bordered layout="horizontal" size="small" column={columnsCount}>
                                    <Descriptions.Item label="x" span={columnsCount}>{getNumberTable(r.x)}</Descriptions.Item>
                                    <Descriptions.Item label="|x-x*|" span={columnsCount}>{getNumberTable(r.absXMuinusXValue)}</Descriptions.Item>
                                    <Descriptions.Item label="p">{r.p.toFixed(fxd)}</Descriptions.Item>
                                    <Descriptions.Item label="x*">{r.xValue.toFixed(fxd)}</Descriptions.Item>
                                    <Descriptions.Item label="s*">{r.s.toFixed(fxd)}</Descriptions.Item>
                                    <Descriptions.Item label="sigma">{r.сhr.toFixed(fxd)}</Descriptions.Item>
                                </Descriptions>
                                {r.reportIterations.map( (rep, idx) => {
                                    return <Descriptions key={'r'+idx} bordered layout="horizontal" size="small" column={columnsCount}>
                                        <Descriptions.Item label="x*i" span={columnsCount}>{getNumberTable(rep.xI)}</Descriptions.Item>
                                        <Descriptions.Item label="X*/p" span={columnsCount}>{getNumberTable(rep.xValueDivideP)}</Descriptions.Item>
                                        <Descriptions.Item label="X*/p" span={columnsCount}>{getNumberTable(rep.arrXiMinXValueSquared)}</Descriptions.Item>
                                        <Descriptions.Item label="p-1">{rep.pMinusOne.toFixed(fxd)}</Descriptions.Item>
                                        <Descriptions.Item label="x*">{rep.xValue.toFixed(fxd)}</Descriptions.Item>
                                        <Descriptions.Item label="s*">{rep.sValue.toFixed(fxd)}</Descriptions.Item>
                                        <Descriptions.Item label="sigma">{rep.chr.toFixed(fxd)}</Descriptions.Item>
                                        <Descriptions.Item label="x*-sigma">{rep.xValueMinusChr.toFixed(fxd)}</Descriptions.Item>
                                        <Descriptions.Item label="x*+sigma" span={3}>{rep.xValuePlusChr.toFixed(fxd)}</Descriptions.Item>
                                    </Descriptions>
                                })}
                            </Space>
                    </Col>
                    </Row>
            </div>)
        })}
    </>)
};

