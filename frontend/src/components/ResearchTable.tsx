import React, { useContext, useState, useEffect, useRef } from 'react'
import { TypesOfTests } from '../services/entities/typesOfTests'
import { Table, Form, InputNumber } from 'antd/es'
import { ResearchSample } from '../services/entities/research';
import { ResearchResult } from '../services/entities/research-results';
import { ColumnsType } from 'antd/es/table';

const EditableContext = React.createContext<any>({});

interface EditableRowProps {
    index: number;
}

interface EditableCellProps {
    title: React.ReactNode;
    editable: boolean;
    children: React.ReactNode;
    dataIndex: string;
    record: any;
    handleSave: (record: any) => void;
}

export interface OnResultChangeResponse {
    sample: number
    types_of_test: number
    measurementResult: number
    testNumber: number
}

export interface ResearchTableProps {
    samples: ResearchSample[],
    typesOfTests: TypesOfTests[],
    results?: ResearchResult[],
    numberOfTests: number,
    onResultChange: (response: OnResultChangeResponse) => void
}
  
const EditableRow: React.FC<EditableRowProps> = ({ index, ...props }) => {
    const [form] = Form.useForm();
    return (
        <Form form={form} component={false}>
            <EditableContext.Provider value={form}>
                <tr {...props} />
            </EditableContext.Provider>
        </Form>
    );
};

const EditableCell: React.FC<EditableCellProps> = ({
    title,
    editable,
    children,
    dataIndex,
    record,
    handleSave,
    ...restProps
  }) => {
      const [editing, setEditing] = useState(false);
      const inputRef = useRef<any>();
      const form = useContext(EditableContext);
    
      useEffect(() => {
        if (editing) {
          inputRef.current.focus();
        }
      }, [editing]);
    
      const toggleEdit = () => {
          setEditing(!editing);
          form.setFieldsValue({ [dataIndex]: record[dataIndex] });
      };
    
      const save = async (e:React.KeyboardEvent<HTMLInputElement> | React.FocusEvent<HTMLInputElement>) => {
          try {
            const values = await form.validateFields();
            toggleEdit();
            handleSave({ row: record, values });
          } catch (errInfo) {
            console.log('Save failed:', errInfo);
          }
      };
    
      let childNode = children;
      if (editable)
      childNode = editing ? (
          <Form.Item
              style={{ margin: 0 }}
              name={dataIndex}
          >
              <InputNumber ref={inputRef} onPressEnter={save} onBlur={save}  />
          </Form.Item>
          ) : (
          <div className="editable-cell-value-wrap" style={{ paddingRight: 24 }} onClick={toggleEdit}>
              {children}
          </div>
          );
    
      return <td {...restProps}>{childNode}</td>;
};

function ResearchTable(props: ResearchTableProps) {
    const { samples, typesOfTests, results, numberOfTests, onResultChange } = props;
    const [rows, setRows] = useState<any[]>([]);
    const components = {
        body: {
          row: EditableRow,
          cell: EditableCell,
        },
      };

    useEffect(()=>{
        if (results)
            setRows(getRows(results));
        
    },[props.results])

    const saveResult = (data: any) => {
        const { row, values } = data;
        try {
            const resData = row[Object.keys(values)[0]+'o'];
            resData.measurementResult = values[Object.keys(values)[0]];
            const res:OnResultChangeResponse = {
                measurementResult: resData.measurementResult || null,
                sample: resData.sample.id,
                types_of_test: resData.types_of_test.id,
                testNumber: resData.testNumber
            }
            if (res)
                onResultChange(res);
        } catch (error) {
            console.log(error);
        }
    }

    const getColumns = (samples: ResearchSample[], typesOfTests: TypesOfTests[]):ColumnsType => {
        const columns:ColumnsType = [];
        columns.push({
            title: "№",
            dataIndex: "testNumber",
            key: "testNumber",
        })
        typesOfTests.map((type) => {
            let col:any = {};
            col.title = type.Name
            col.children = samples.map( sample => ({title: sample.name, dataIndex: 't'+type.id+'s'+sample.id, key: 't'+type.id+'s'+sample.id }) );
            col.children = col.children.map( (col: any) => {
                return {
                  ...col,
                  editable: true,
                  onCell: (record: any) => ({
                    record,
                    editable: true,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    handleSave: saveResult,
                  }),
                };
            });
            columns.push(col);
          }
        );
        return columns;
    };

    const cols = getColumns(samples, typesOfTests)

    const getRows = (results: ResearchResult[]) => {
        const rows = [];
        for (let i = 1; i <= numberOfTests; i++) {
            let row:any = {};
            typesOfTests.map((type) => {
                samples.map( sample => {
                    row.testNumber = i;
                    row.key=row.testNumber+'t'+type.id+'s'+sample.id+'k';
                    const result = results.find( (el) => el.sample.id === sample.id && el.testNumber === i && el.types_of_test.id===type.id);
                    if (result){
                        row['t'+type.id+'s'+sample.id+'o'] = result;
                        row['t'+type.id+'s'+sample.id] = result?.measurementResult || 0;
                    }
                    else{
                        const r:any = {
                            sample: {id:sample.id},
                            testNumber: i,
                            types_of_test: {id: type.id},
                            measurementResult: ""
                        }
                        row['t'+type.id+'s'+sample.id+'o'] = r;
                        row['t'+type.id+'s'+sample.id] = r.measurementResult;
                    }
                })
            })
            
            rows.push(row);
        }
        return rows;
    }
    
    return (
        <div className="researches-table">
            <Table 
                pagination={false} 
                bordered size="small" 
                dataSource={rows} 
                components={components} 
                columns={cols} 
                rowClassName={() => 'editable-row'}
            ></Table>
        </div>
    )
}

export default ResearchTable;
