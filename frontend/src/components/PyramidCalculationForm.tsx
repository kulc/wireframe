import React, { useState, useEffect } from 'react'
import { Form, InputNumber, Button } from 'antd/es'
import { FormProps } from 'antd/es/form';
import { Store } from 'antd/es/form/interface';
import { PyramidParams } from '../services/calc/PyramidCalculation';

export interface PyramidCalculationFormProps {
    formProps?: FormProps
    onFinish?: (values: PyramidParams) => void
}

export default function PyramidCalculationForm(props: PyramidCalculationFormProps) {
    const [paymentPerMonth, setPaymentPerMonth] = useState<number>(0);
    const [form] = Form.useForm();
    useEffect(()=>{
        form.validateFields().then(values => getPaymentPerMonth(values.average_salary, values.tax, values.meal))
    }, [form]);
    const onValuesChange = (changedValues: Store, values: Store) => {
        getPaymentPerMonth(values.average_salary, values.tax, values.meal);
    }
    const getPaymentPerMonth = (average_salary: number, tax: number, meal: number) => {
        setPaymentPerMonth(average_salary - (average_salary / 100 * tax) - (average_salary / 100 * meal));
        return average_salary - (average_salary / 100 * tax) - (average_salary / 100 * meal)
    }
    const onFinish = (values: any) => {
        props.onFinish && props.onFinish({...values, paymentPerMonth});
    }
    return (
        <Form className="pyramid-form" size="small" onValuesChange={onValuesChange} form={form} onFinish={onFinish} {...props.formProps}>
            <div className="pyramid-form-grid">
                <Form.Item name="average_salary" label="Средняя зарплата"><InputNumber min={1} step={1} autoFocus/></Form.Item>
                <Form.Item name="tax" label="Налог с зарплаты"><InputNumber min={1} step={1} /></Form.Item>
                <Form.Item name="meal" label="Затраты с каждой зарплаты"><InputNumber min={1} step={1} /></Form.Item>
                <Form.Item name="earth_population" label="Популяция"><InputNumber min={1} step={1} /></Form.Item>
                <Form.Item name="people_per_day" label="Прирост людей в день"><InputNumber min={1} step={1} /></Form.Item>
            </div>
            <Form.Item label="Ежемесячный платеж">{paymentPerMonth.toFixed(2)}</Form.Item>
            <Form.Item><Button type="primary" htmlType="submit">Применить</Button></Form.Item>
        </Form>
    )
}
