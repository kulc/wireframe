import React, { Suspense } from 'react';
import { ROUTES } from '../pages/routes';
import { Layout } from 'antd/es';
import '../App.scss';
import { Route, Switch } from 'react-router-dom';
import AppMenu from './AppMenu';
import SuspenseFallback from './SuspenseFallback';

const { Header, Footer, Content } = Layout;

export const PageLayout = () => {
    return (
        <Layout className="layout-container">
            <Header>
                <AppMenu />
            </Header>
            <Content style={{ padding: '0 50px' }}>
                <Suspense fallback={<SuspenseFallback />}>
                    <div className="content-container" style={{minHeight: 240}}>
                        <Switch>
                            {Object.values(ROUTES).map((route) => (
                                <Route {...route} />
                            ))}
                        </Switch>
                    </div>
                </Suspense>
            </Content>
            <Footer>&copy; 2020</Footer>
        </Layout>
)};