import React, { useContext, useState } from 'react';
import { Location } from 'history';
import { ROUTES, ROUTE_KEYS } from '../pages/routes';
import { Menu, Avatar } from 'antd/es';
import { UserOutlined, LoginOutlined, LogoutOutlined } from '@ant-design/icons';
import { matchPath, RouteProps, Link, withRouter, RouteComponentProps } from 'react-router-dom';
import AuthModal from './AuthModal';
import { UserContext } from '../services/contexts/userContext';
import { USER_TOKEN_NAME } from '../constants/common';
import { Roles } from '../services/entities/user';

const AppMenu = ({location}: RouteComponentProps) =>{
    const [showAuthModal, setShowAuthModal] = useState<boolean>(false);
    const userContextData = useContext(UserContext);
    const user = userContextData?.user;
    
    const getMatchedKey = (location : Location) => {
        let loc = Object.values(ROUTES).find((route) =>
            matchPath(location.pathname, route)
        ) || {} as RouteProps;
        let locpath = loc.path;        
        if (typeof (locpath) == "string") {
            locpath = [locpath];
        }
        return locpath;
    }
    const onClickLogout = () => {
        userContextData?.logout && userContextData.logout();
        localStorage.removeItem(USER_TOKEN_NAME);
        setShowAuthModal(false);
    }
    return (
        <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={[ROUTE_KEYS.ROOT]}
            selectedKeys={getMatchedKey(location)}
        >
            <Menu.Item key={ROUTE_KEYS.ROOT}><Link to='/'>Главная</Link></Menu.Item>
            <Menu.Item key={ROUTE_KEYS.PYRAMID}><Link to={ROUTE_KEYS.PYRAMID}>Пирамида</Link></Menu.Item>
            {user && user.role.type === Roles.Laborant && user.organization?.id && <Menu.Item key={ROUTE_KEYS.LABORATORY}><Link to={ROUTE_KEYS.LABORATORY+'/'+user.organization.id}>Исследования</Link></Menu.Item>}
            {user && user.role.type === Roles.Coordinate && <Menu.Item key={ROUTE_KEYS.LABORATORYLIST}><Link to={ROUTE_KEYS.LABORATORYLIST}>Лаборатории и исследования</Link></Menu.Item>}
            {user && user.role.type === Roles.Coordinate && <Menu.Item key={ROUTE_KEYS.QUANTITYCALCULATION}><Link to={ROUTE_KEYS.QUANTITYCALCULATION}>Расчет количества лабораторий</Link></Menu.Item>}
            { user ? (
                <Menu.SubMenu style={{float: "right"}} title={<><Avatar icon={<UserOutlined/>}/> {user.username}</>}>
                    <Menu.ItemGroup title={"Роль: "+user.role.name}></Menu.ItemGroup>
                    <Menu.Divider />
                    <Menu.Item onClick={onClickLogout}><LogoutOutlined/>Выйти</Menu.Item>
                </Menu.SubMenu>
            ) : (
                <Menu.Item style={{float: "right"}}>
                    <div onClick={()=>{setShowAuthModal(true)}}><LoginOutlined/>Войти</div>
                    <AuthModal showLoginModal={showAuthModal} onCancel={()=>{setShowAuthModal(false)}} />
                </Menu.Item>
            )}
        </Menu>
    )
}

export default withRouter(AppMenu);