import React from 'react'
import { Spin } from 'antd/es';

export default () => {
    return <Spin style={{margin: "24px"}} spinning={true} size="large" tip="Идет загрузка"></Spin>
};
