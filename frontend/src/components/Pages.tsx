import React, {MouseEvent, useState, useEffect} from 'react';
import { Button, Spin, message, Empty } from 'antd/es';
import { LoadingOutlined } from '@ant-design/icons';
import { getPages } from '../services/api/pages';
import PageList from '../components/PageList';
import { Page } from '../services/api/models';

export const Pages = () => {
    const [pages, setPages] = useState<Page[]>();
    const [pending, setPending] = useState<Boolean>(false);
    const loadPages = async () => {
        setPending (true);
        const mkey = "get_pages";
        message.loading({content: "Загрузка", key: mkey});
        try {
            setPages (await getPages());
            setPending(false);
            message.success({content: "Страницы загружены", key: mkey, duration: 1});
        } catch (error) {
            setPending(false);
            message.error({content: "Страницы не загружены", key: mkey, duration: 2});
        }
    }
    useEffect( () => {loadPages()}, []);
    
    const onClick = async (e: MouseEvent) => {
        e.preventDefault();
        loadPages();
    }
    const Loading = <Spin indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />}/>;
    const WaitLoading = <Empty description="Страницы не загружены"><Button onClick={onClick} type="primary">Загрузить</Button></Empty>
    const PList = (pages) ? <PageList pages={pages}></PageList> : (pending) ? Loading : WaitLoading;
    
    return (
        <>
            <h1>Страницы</h1>
            {PList}
        </>
    )
}