import React, { useState } from 'react';
import { Form, Button, Table, Row, Col, InputNumber } from 'antd/es';
import { Aw, A, A_R, Ar } from '../services/calc/msi';
import { ColumnProps } from 'antd/es/table';

interface FormFields {
    count_labs: number
    count_test_types: number
    count_samples: number
    count_trials: number
}
interface Uncert {
    y: number,
    Aw: number,
    A: number,
    A_R: number,
    Ar: number
}
export default () => {
    const [fields] = useState([
        {name: ['count_labs'], value: 2 },
        {name: ['count_test_types'], value: 3 },
        {name: ['count_samples'], value: 3 },
        {name: ['count_trials'], value: 15 },
    ]);
    const [tableData, setTableData] = useState<any>();

    const onFinish = (values: any) => {
        const fields = values as FormFields;
        let uncertainties = new Array<{}>();
        for (let i = 2; i <= fields.count_trials; i++){
            if (i % fields.count_samples === 0){
                uncertainties.push({
                    key: i/fields.count_samples,
                    y: i/fields.count_samples,
                    Aw: Aw(i),
                    A: A(fields.count_labs, i),
                    Ar: Ar(fields.count_labs, i),
                    A_R: A_R(fields.count_labs, i)
                });
            }
        }

        const columns : ColumnProps<Uncert>[] = [
            {title: "Y", dataIndex: "y", key: "0"},
            {title: "Aw", dataIndex: "Aw", key: "1"},
            {title: "A", dataIndex: "A", key: "2"},
            {title: "Ar", dataIndex: "Ar", key: "3"},
            {title: "A_R", dataIndex: "A_R", key: "4"},
        ];
        setTableData ({dataSource: uncertainties, columns})
    }

    const layout = {
        labelCol: { lg: 14, xl: 10, md: 16 },
    };
    const tailLayout = {wrapperCol: {offset: 10, span: 16}};

    return (<Row gutter={[16,16]}>
        <Col xs={12}>
            <h2>Расчет количества лабораторий</h2>
            <Form
                {...layout}
                name="define_number_of_tests_form"
                onFinish={onFinish}
                fields={fields}
            >
                <Form.Item
                    label="Количество лабораторий"
                    name="count_labs"
                    rules={[{ required: true}]}
                    
                    >
                    <InputNumber autoFocus />
                </Form.Item>
                <Form.Item
                    label="Количество видов исследований"
                    name="count_test_types"
                    rules={[{ required: true}]}
                    >
                    <InputNumber />
                </Form.Item>
                <Form.Item
                    label="Количество образцов"
                    name="count_samples"
                    rules={[{ required: true}]}
                    >
                    <InputNumber />
                </Form.Item>
                <Form.Item
                    label="Количество измерений образца"
                    name="count_trials"
                    rules={[{ required: true}]}
                    >
                    <InputNumber />
                </Form.Item>
                <Form.Item {...tailLayout}>
                    <Button size="large" type="primary" htmlType="submit">
                    Расчитать
                    </Button>
                </Form.Item>
            </Form>
        </Col>
    {(tableData && (<Col xs={12}><h3>Результаты</h3><Table pagination={false} size="small" bordered rowClassName={(rec) => (rec.Aw > 0.92) && "ant-alert-error"} {...tableData} /></Col>))}
    </Row>)
}