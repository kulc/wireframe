import React, { useState } from 'react';
import { Store } from 'antd/es/form/interface';
import { Form, Button, InputNumber } from 'antd/es';

export interface AccuracyFormProps {
    defaultAccuracy?: number
    onAccuracySubmit: (accuracy: number) => void
}

export const AccuracyForm = (props: AccuracyFormProps) => {
    const [fields] = useState([
        {name: ['accuracy'], value: props.defaultAccuracy || 0.01 }
    ]);
    
    const onFinish = (values: Store) => {
        props.onAccuracySubmit(values.accuracy);
    }

    return (
        <Form onFinish={onFinish} name="set_accuracy" fields={fields} layout="inline">
            <Form.Item name="accuracy" label="Точность вычисления" rules={[{ required: true}]}><InputNumber autoFocus step={0.01} min={0.01}/></Form.Item>
            <Form.Item><Button htmlType="submit" type="primary">Ok</Button></Form.Item>
        </Form>
        )
}