import { lazy } from 'react';
import { RouteProps } from 'react-router-dom';
const Pyramid = lazy(() => import('./Pyramid'));
const Home = lazy(() => import('./Home'));
const NotFound = lazy (()=> import('./NotFound'));
const Laboratory = lazy (()=> import('./Laboratory'));
const NumberOfTests = lazy (()=> import('../components/NumberOfTests'));
const LaboratoryList = lazy (()=> import('./LaboratoryList'));
const Reports = lazy (()=> import('./Reports'));

export const ROUTE_KEYS : {[key:string]: string} = {
    ROOT: "/",
    NOT_FOUND: "/404",
    LABORATORY: "/laboratory",
    LABORATORYLIST: "/laboratory-list",
    QUANTITYCALCULATION: '/quantity-calculation',
    REPORTS: '/researches/:id/reports',
    PYRAMID: '/pyramid'
};

interface ExtendedRoute extends RouteProps { key: string } ;

export const ROUTES : {[key:string]: ExtendedRoute} = 
{
    ROOT: {
        component: Home,
        exact: true,
        path: ROUTE_KEYS.ROOT,
        key: ROUTE_KEYS.ROOT,
    },
    LABORATORY: {
        component: Laboratory,
        exact: true,
        path: ROUTE_KEYS.LABORATORY+"/:id",
        key: ROUTE_KEYS.LABORATORY,
    },
    LABORATORYLIST: {
        component: LaboratoryList,
        exact: true,
        path: ROUTE_KEYS.LABORATORYLIST,
        key: ROUTE_KEYS.LABORATORYLIST,
    },
    QUANTITYCALCULATION: {
        component: NumberOfTests,
        exact: true,
        path: ROUTE_KEYS.QUANTITYCALCULATION,
        key: ROUTE_KEYS.QUANTITYCALCULATION,
    },
    REPORTS: {
        component: Reports,
        exact: true,
        path: ROUTE_KEYS.REPORTS,
        key: ROUTE_KEYS.REPORTS,
    },
    PYRAMID: {
        component: Pyramid,
        exact: true,
        path: ROUTE_KEYS.PYRAMID,
        key: ROUTE_KEYS.PYRAMID,
    },
    NOT_FOUND: {
        component: NotFound,
        key:  ROUTE_KEYS.NOT_FOUND
    },
};