import React, { useState } from 'react'
import PyramidCalculationForm from '../components/PyramidCalculationForm'
import { Row, Col, Typography } from 'antd/es';
import { pyramidCalculation, pyramidCalculationResult, PyramidParams } from '../services/calc/PyramidCalculation';
import { Line as Ln, LinearComponentProps } from 'react-chartjs-2';
import { LineChart, XAxis, YAxis, CartesianGrid, Legend, Tooltip, Line, ResponsiveContainer } from 'recharts';

export default function Pyramid() {
    const [fields] = useState([
        {name: ['average_salary'], value: 10000 },
        {name: ['tax'], value: 13 },
        {name: ['meal'], value: 22 },
        {name: ['earth_population'], value: 150*1000*1000 },
        {name: ['people_per_day'], value: 10 },
    ]);
    const [moneyChartData, setMoneyChartData] = useState<LinearComponentProps | undefined>();
    const [investorChartData, setInvestorChartData] = useState<LinearComponentProps | undefined>();
    const [lineChartData, setLineChartData] = useState<Array<pyramidCalculationResult> | undefined>();
    const onParamsApply = (values: PyramidParams) => {
        const pyramidCalcs = pyramidCalculation(values)
        setLineChartData(pyramidCalcs);
        const mData = getMoneyChartData(pyramidCalcs);
        const iData = getInvestorChartData(pyramidCalcs);
        setMoneyChartData({data: mData});
        setInvestorChartData({data: iData});
    }
    const getInvestorChartData = (values: pyramidCalculationResult[]) => {
        // console.log(values[values.length-1].investors_accounts);
        console.log(values.map(v => v.gathered_money).reduce( (v, p) => v + p));
        console.log(values.map(v => v.organizer_account).reduce( (v, p) => v + p));
        console.log(values.map(v => v.investors_count).reduce( (v, p) => v + p));
        
        return {
            labels: values.map( v => v.day ),
            datasets: [{
                label: 'Количество инвесторов',
                data: values.map( v => v.investors_count.toFixed(0) ),
            }]
        }
    }
    const getMoneyChartData = (values: pyramidCalculationResult[]) => {
        let datasets = [];
        datasets.push({
            label: 'Счет организатора',
            data: values.map( v => v.organizer_account.toFixed(2) ),
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)'
        })
        datasets.push({
            label: 'Счет пирамиды',
            data: values.map( v => v.gathered_money.toFixed(2) )
        })
        return {
            labels: values.map( v => v.day ),
            datasets
        }
    }
    const CustomTooltip = ({ active, payload, label }: {active: boolean, payload: [any], label: string}) => {
        if (active && payload){
            return <>
                <p>Счет пирамиды: {payload.filter( p => p.dataKey==="gathered_money")[0].value.toLocaleString('ru-RU', { style: 'currency', currency: 'RUB' })}</p>
                <p>Счет организатора: {payload.filter( p => p.dataKey==="organizer_account")[0].value.toLocaleString('ru-RU', { style: 'currency', currency: 'RUB' })}</p>
            </>
        }
        return <></>
    }
    return (
        <>
            <Typography.Title>Пирамида</Typography.Title>
            <Row>
                <Col>
                    <Typography.Title level={2}>Параметры</Typography.Title>
                    <PyramidCalculationForm onFinish={onParamsApply} formProps={{fields}}/>
                    <ResponsiveContainer width="100%" height={300}>
                        <LineChart data={lineChartData} >
                            <XAxis dataKey="day"/>
                            <YAxis/>
                            <CartesianGrid strokeDasharray="3 3"/>
                            <Tooltip content={(props:any) => <CustomTooltip active={props.active} payload={props.payload} label={props.label} />}/>
                            <Legend payload={[{id: 'gathered_money', value: 'Счет пирамиды', type: "circle", color: '#82ca9d' }, {id: 'organizer_account', value: 'Счет организатора', type: "circle", color: '#8884d8' }]} />
                            <Line type="monotone" dataKey="organizer_account" stroke="#8884d8" activeDot={{ r: 8 }} />
                            <Line type="monotone" dataKey="gathered_money" stroke="#82ca9d" />
                        </LineChart>
                    </ResponsiveContainer>
                    { moneyChartData && <Ln data={moneyChartData.data} /> }
                    { investorChartData && <Ln data={investorChartData.data} /> }
                </Col>
            </Row>
        </>
    )
}
