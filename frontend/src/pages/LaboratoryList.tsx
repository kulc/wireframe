import React, { useContext } from 'react'
import { LaboratoryListComponent } from '../components/LaboratoryList'
import { UserContext } from '../services/contexts/userContext';
import { Roles } from '../services/entities/user';
import { Alert } from 'antd/es';

export default () => {
    const userContextData = useContext(UserContext);
    if (userContextData?.user?.role.type !== Roles.Coordinate)
        return <Alert message="Требуется авторизация или нет доступа" type="error" showIcon />
    
    return <LaboratoryListComponent />;
}