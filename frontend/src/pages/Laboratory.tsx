import React, { useContext } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { MatchProps as matchProps } from './interfaces';
import { LaboratoryView } from '../components/LaboratoryView';
import { Alert, Tabs } from 'antd/es';
import { ResearchView } from '../components/ResearchView';
import { SamplesView } from '../components/SamplesView';
import { UserContext } from '../services/contexts/userContext';
import { Roles } from '../services/entities/user';

export default (props: RouteComponentProps<matchProps>) => {
    const userContextData = useContext(UserContext);
    const org = props.match.params?.id && +props.match.params?.id;
    if (!org)
        return <Alert message="Не указана организация" type="error" showIcon />
    const noAuth = <Alert message="Требуется авторизация или нет доступа" type="error" showIcon />
    const canView = (userContextData?.user?.role.type === Roles.Coordinate || userContextData?.user?.organization?.id === org ? true : false)
    
    if (!userContextData?.user?.id || !canView)
        return noAuth

    return (
        <>
            <LaboratoryView id={org} />
            <Tabs type="card">
                <Tabs.TabPane tab="Исследования" key="1"><ResearchView orgId={org} /></Tabs.TabPane>
                <Tabs.TabPane tab="Образцы" key="2"><SamplesView orgId={org} /></Tabs.TabPane>
            </Tabs>
        </>
    )
}