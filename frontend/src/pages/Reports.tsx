import React, { useState, useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { MatchProps as matchProps } from './interfaces';
import { Alert, Spin, Button, Empty, Space } from 'antd/es';
import { AccuracyForm } from '../components/AccuracyForm';
import { getResearchById } from '../services/api/researches';
import { Research } from '../services/entities/research';
import { getResults } from '../services/api/research-results'
import { ResearchResult } from '../services/entities/research-results';
import { numberSameValues } from '../services/calc/msi';
import { ReportByTestType, ReportIteration, ReportView } from '../components/ReportView';

export default (props: RouteComponentProps<matchProps>) => {
    const [isLoading, setIsloading] = useState<boolean>(false);
    const [accuracy, setAccuracy] = useState<number>(0.01);
    const [research, setResearch] = useState<Research>();
    const [results, setResults] = useState<ResearchResult[]>();
    const [report, setReport] = useState<ReportByTestType[]>();
    const idResearch = props.match.params.id ? +props.match.params.id : undefined;
    useEffect(()=>{
        initResearch(idResearch);
    },[idResearch]);

    const initResearch = async (idResearch?: number) => {
        if (idResearch){
            setIsloading(true)
            getResearchById(idResearch).then(async rs => {
                if (rs){
                    setResearch(rs)
                    setResults(await getResults({research: idResearch}))
                }
            }).finally(()=>setIsloading(false));
        }
    }

    if (!idResearch)
        return <Alert message="Не указано исследование" type="error" showIcon />
    
    const onAccuracySubmit = (_accuracy: number) => {
        setAccuracy(_accuracy)
    }

    const createReport = () => {
        if (!research) return null;
        if (!results) return null;
        setIsloading(true);
        const report:ReportByTestType[] = [];
        const sValueConst = 1.483;
        const chrConst = 1.5;
        const sConst = 1.134;
        const uConst = 1.25;

        research.types_of_tests.forEach( (t) => {
            const   typeResults = results.filter(r => r.types_of_test.id === t.id).sort( (a,b)=> a.measurementResult - b.measurementResult);
            const   xValue = numberSameValues(typeResults.map(tr => tr.measurementResult));
            let sValue = sValueConst * xValue;
            const   сhr = chrConst * sValue,
                    x = typeResults.map(tr => tr.measurementResult),
                    p = typeResults.length,
                    absXMuinusXValue = typeResults.map(tr => Math.abs(tr.measurementResult - xValue));
            const reportIterations = Array<ReportIteration>();

            const r:ReportByTestType = {
                typeOfTest: t,
                x,
                p,
                xValue,
                absXMuinusXValue,
                s: sValue,
                сhr,
                reportIterations
            }
            let ssValue;
            let XI = r.x;
            let xV: number = r.xValue;
            let s:number;
            let chr: number = r.сhr;

            do {
                const res:any = {};
                const xValueMinusChr = xV - chr;
                const xValuePlusChr = xV + chr;
                res.xValueMinusChr = xValueMinusChr;
                res.xValuePlusChr = xValuePlusChr;
                const xI = XI.map (x => {
                    if (x < xValueMinusChr) return xValueMinusChr;
                    if (x > xValuePlusChr) return xValuePlusChr;
                    if ((xValueMinusChr <= x) && (x <= xValuePlusChr)) return x;
                    return 0;
                }) as number[];
                res.xI = xI;
                XI = xI;
                const xValueDivideP: number[] = xI.map(x => x / xI.length);
                res.xValueDivideP = xValueDivideP;
                xV = xValueDivideP.reduce( ( p, c ) => p + c, 0 );
                res.xValue = xV;
                const pMinusOne: number = XI.length - 1;
                res.pMinusOne = pMinusOne;
                const arrXiMinXValueSquared: number[] = xI.map ( x => Math.pow(x-xV, 2) );
                res.arrXiMinXValueSquared = arrXiMinXValueSquared;
                const sGRs = arrXiMinXValueSquared.reduce( ( p, c ) => p + c, 0 ) / pMinusOne;
                s = sConst * (Math.sqrt( sGRs ))
                res.s = s;
                chr = chrConst * s;
                res.chr = chr;
                ssValue = Math.abs(sValue - s)
                sValue = s;
                res.sValue = sValue;
                r.reportIterations.push(res)
            } while (ssValue > accuracy);

            const uX = uConst * sValue / Math.sqrt(r.x.length);
            r.uX = uX;

            const labSred = research.participantOrgs.map ( org => {
                let zindex: number;
                const orgResults = results
                        .filter(r => r.organization.id === org.id && r.types_of_test.id === t.id)
                        .map(r=>r.measurementResult)
                const orgResSum = orgResults.reduce( ( p, c ) => p + c, 0 );
                if (uX <= 0.3 * sValue){
                    zindex = ((orgResSum / orgResults.length) - xV) / sValue
                }
                else {
                    zindex = ((orgResSum / orgResults.length) - xV) / Math.sqrt( sValue * sValue + uX * uX )
                }
                return {
                    org,
                    zindex
                }
            })
            r.zindex = labSred
            report.push(r);
        })
        setReport(report);
        setIsloading(false);
    }

    return (<>
        <Space direction="vertical">
        <h1>Отчеты</h1>
            <AccuracyForm onAccuracySubmit={onAccuracySubmit} defaultAccuracy={accuracy} />
            <Spin tip="Загрузка результатов исследования" spinning={isLoading}>
                <Button onClick={createReport}>Сформировать отчет</Button>
                { report ? <ReportView reportByTestTypes={report} /> : <Empty description="Отчет не сформирован" /> }
            </Spin>
        </Space>
    </>)
}
