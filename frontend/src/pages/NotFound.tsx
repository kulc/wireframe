import React from 'react';
import { Button, Result } from 'antd/es';
import { Link } from 'react-router-dom';

export default () => (
        <Result
            status="404"
            title="404"
            subTitle="Извините, запрашиваемая страница не существует"
            extra={<Button type="primary"><Link to='/'>На главную</Link></Button>}
        />
)