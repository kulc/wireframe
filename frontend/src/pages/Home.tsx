import React from 'react';
import { Result } from 'antd/es';

export default () => {
    return (<Result 
            status="403"
            title="Добро пожаловать"
            subTitle="Для начала работы авторизуйтесь и выберете пункт меню"/>)
}