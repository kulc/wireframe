import React from 'react';
import {UserInfo} from '../entities/user';

interface IUserContext {
    user?: UserInfo
    login?: (user: UserInfo) => void,
    logout?: () => void
}

const UserContext = React.createContext<IUserContext|undefined>(undefined);

export { UserContext };