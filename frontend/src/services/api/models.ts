export interface Page {
    id:   string,
    Name: string,
    Body: string,
    Somecomponent: Somecomponent
}

export interface Somecomponent {
    id: string,
    compname: string
}