export interface Error{
    code: number
    message: string
}

export interface Message {
    id: string
    message: string
}

export interface StatusMessage {
    error: string,
    message: string,
    statusCode: number
}