import { API_URL } from './utility';
import { ApiClient } from './apiClient';
import { ResearchResult, NewResearchResult, GetResultsRequest } from '../entities/research-results';
import { StatusMessage } from './interfaces';

export const getResults = (request: GetResultsRequest):Promise<ResearchResult[]> => {
    return ApiClient.get(API_URL + ApiClient.composeURL("/research-results", request));
}

export const getResultsByResearchAndOrg = (research: number, organization: number):Promise<ResearchResult[]> => {
    return ApiClient.get(API_URL + "/research-results?research="+research+'&sample.organization='+organization);
}

export const updateResult = (id: number, request: NewResearchResult):Promise<StatusMessage|null> => {
    return ApiClient.put(API_URL + "/research-results/"+id, request);
}

export const createResult = (result: NewResearchResult):Promise<StatusMessage|null> => {
    return ApiClient.post(API_URL + "/research-results", result);
}