import { API_URL } from './utility';
import { AuthLocalLogin, UserLoginResponse, UserInfo } from '../entities/user';
import { ApiClient, APIError } from './apiClient';

export const login = (logindata: AuthLocalLogin):Promise<UserLoginResponse> => 
    ApiClient.post(API_URL + "/auth/local", logindata);

export const checkAuth = async ():Promise<UserInfo> => {
    try {
        const authed = await ApiClient.get(API_URL + "/users/me");
        return await getUserInfo(authed.id);
    } catch (error) {
        throw new APIError(error, null);
    }
};

export const getUserInfo = (id:number):Promise<UserInfo> => ApiClient.get(API_URL + "/users/"+id);