export const API_URL = "" //"http://localhost:1337" //process.env.REACT_APP_API_URL

const checkForError = (response:Response) => {
  if (!response.ok) throw Error(response.statusText);
  return response.json();
};

interface HttpResponse<T> extends Response {
    parsedBody?: T;
}

export async function http<T>(
    request: RequestInfo
  ): Promise<HttpResponse<T>> {
    return await fetch(request)
      .then(checkForError)
      .then(response => response.parsedBody = response.json())
      .catch(error => error);
};

export async function get<T>(
    path: string,
    args: RequestInit = { method: "get" }
  ): Promise<HttpResponse<T>> {
    return await http<T>(new Request(path, args));
};
  
export async function post<T>(
    path: string,
    body: any,
    args: RequestInit = { method: "post", body: JSON.stringify(body), headers: { 
      'Accept': 'application/json', 
      'Content-Type': 'application/json' } }
  ): Promise<HttpResponse<T>>  {
    return await http<T>(new Request(path, args));
};
  
export async function put<T>(
    path: string,
    body: any,
    args: RequestInit = { method: "put", body: JSON.stringify(body) }
  ): Promise<HttpResponse<T>> {
    return await http<T>(new Request(path, args));
};