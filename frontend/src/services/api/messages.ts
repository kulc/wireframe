import { Message } from "./interfaces";

export const messages : Message[] = [
    {
        id: "Auth.form.error.invalid", 
        message: "Identifier or password invalid."
    }
]