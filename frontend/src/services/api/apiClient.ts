import { saveAs } from 'file-saver';
import { HTTP_STATUS, USER_TOKEN_NAME } from '../../constants/common';


const qs = require('query-string-object');

/**
 * @class
 * @description REST API request client
 */
export class ApiClient {
  static instance: ApiClient;

  /**
   * Create ApiClient instance
   */
  public static createClient() {
      ApiClient.instance = new ApiClient();
  }

  public static download(url: string, filename: string) {
      return fetch(url, { method: 'GET' }).then(res => res.blob()).then(blob => saveAs(blob, filename));
  }

  private static getHeader = () => {
    let header;
    if (localStorage.getItem(USER_TOKEN_NAME)){
        header = {'Authorization': `Bearer ${localStorage.getItem(USER_TOKEN_NAME)}`};
    }
    return header;
  }

  /**
   *
   * @description method of creating a GET request
   * @param url path of request
   * @returns {Promise<Response>}
   */
  public static get(url: string) {
      // такой фетч нужен для сбрасывания кеширования
      // return fetch((`${url}?${new Date().getTime()}`), {
      return fetch(`${url}`, {
          method: 'GET',
          headers: this.getHeader()
      })
          .then(this.handleErrors)
          .then(response => response.json())
          .catch((err) => {
              throw new APIError(err, null);
          });
  }

  /**
   *
   * @description method of creating a POST request
   * @param url url path of request
   * @param body object to create a new record
   * @returns {Promise<Response | void>}
   */
  public static post(url: string, body: any) {
      return fetch(`${url}`, {
          headers: {
              'Content-type': 'application/json; charset=UTF-8',
              ...this.getHeader()
          },
          method: 'POST',
          body: JSON.stringify(body)
      })
          .then(this.handleErrors)
          .then(res => res.json());
      // .catch(err => {
      //   throw new APIError(err, null);
      // });
  }

  static handleErrors(response: any) {
      if (!response.ok) {
          const { status } = response;
          if (status === HTTP_STATUS.CODE_401) {
              throw new APIError(null, { body: response, status });
          } else {
              return response.json().then((body: any) => {
                  throw new APIError(null, { body, status });
              });
          }
      }
      return response;
  }

  /**
   *
   * @description method of creating a PUT request,
   * @param url url path of request; the URL must contain the object id
   * @param body object to modify record
   * @returns {Promise<Response | void>}
   */
  public static put(url: string, body: any) {
      return fetch(`${url}`, {
          headers: {
              'Content-type': 'application/json; charset=UTF-8',
              ...this.getHeader()
          },
          method: 'PUT',
          body: JSON.stringify(body)
      })
          .then(this.handleErrors)
          .then(res => res.json())
          .catch((err) => {
              throw new APIError(err, null);
          });
  }

  /**
   *
   * @description method of creating a DELETE request
   * @param url path of request;
   * the URL must contain the object id
   */
  public static delete(url: string) {
      return (
          fetch(`${url}`, {
              method: 'delete',
              headers: this.getHeader()
          })
              .then(this.handleErrors)
          // .then(response => {
          //     return response.json()
          // })
              .catch((err) => {
                  throw new APIError(err, null);
              })
      );
  }

  public static composeURL(url: string, params: any) {
      if (!params) {
          return url;
      }
      const queryParams: string = qs.stringify(params);

      return `${url}?${queryParams}`;
  }
}

/**
 * @class APIError
 * @description API error handling
 */
class APIError {
    /**
   *  ApiResponse
   * @param _error
   * @param response
   */
  // eslint-disable-next-line
    constructor(_error: any, response: any) { }
}

/**
 * Get ApiClient
 * @returns {null|APIClient} ApiClient instance
 */
function getClient() {
    if (!ApiClient.instance) {
        throw new Error('API client hasn\'t been initialized yet');
    }
    return ApiClient.instance;
}

export { getClient, APIError };
