import { API_URL } from './utility';
import { ApiClient } from './apiClient';
import { Samples } from '../entities/samples';

export const getSamplesByOrgId = (orgId?: number):Promise<Samples[]> => {
    return orgId ? ApiClient.get(API_URL + "/samples?organization.id="+orgId) : getSamples();
    
};

export const getSamplesByOrgIdAndResearchId = (orgId: number, researchId: number):Promise<Samples[]> => {
    return orgId ? ApiClient.get(API_URL + "/samples?organization.id="+orgId+"&research.id="+researchId) : getSamples();
    
};

export const getSamples = ():Promise<Samples[]> => ApiClient.get(API_URL + "/samples");