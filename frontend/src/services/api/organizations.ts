import { API_URL } from './utility';
import { ApiClient } from './apiClient';
import { Organization, GetOrganizationsRequest } from '../entities/organization';

export const getOrganization = (id: number):Promise<Organization> => 
    ApiClient.get(API_URL + "/organizations/"+id);

export const getOrganizations = (params: GetOrganizationsRequest):Promise<Organization[]> => 
    ApiClient.get(API_URL + ApiClient.composeURL("/organizations", params));