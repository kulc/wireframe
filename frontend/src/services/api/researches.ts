import { API_URL } from './utility';
import { ApiClient } from './apiClient';
import { Research } from '../entities/research';

export const getResearchesByOrgId = (orgId: number):Promise<Research[]> => ApiClient.get(API_URL + "/researches/byOrgId/"+orgId)

export const getResearches = ():Promise<Research[]> => ApiClient.get(API_URL + "/researches")

export const getResearchById = (id: number):Promise<Research> => ApiClient.get(API_URL + "/researches/" + id)