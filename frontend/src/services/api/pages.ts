import { Page } from './models';
import { API_URL } from './utility';
import { ApiClient } from './apiClient';

export const getPages = ():Promise<Page[]> => ApiClient.get(API_URL + "/pages");
