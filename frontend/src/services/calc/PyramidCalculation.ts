export type pyramidCalculationResult = {
    day: number
    organizer_account: number
    gathered_money: number
    investors_count: number
    investors_accounts: number
}

export type PyramidParams = {
    average_salary: number
    tax: number
    meal: number
    earth_population: number
    people_per_day: number
    paymentPerMonth: number
}

export const pyramidCalculation = (params: PyramidParams):pyramidCalculationResult[] => {
    
    let { people_per_day, earth_population, paymentPerMonth } = params;

    const annual_growth = 0.0030136986;
    const percentage_of_employees = 0.47;
    const monthly_increase = 0.1;
    const organizer_percentage = 0.01;
    const investor_percentage = 0.09;
    const organizer_max = 100*1000*1000;
    const investor_max = 1*1000*1000;
    
    let day = 0;
    let pyramidResult:pyramidCalculationResult[] = [];
    let investors_accounts = 0;
    let gathered_money = 0;
    let organizer_account = 0;
    let investors_count = 0;
    let new_wave_investors_count = 0;
    let previous_investors_count = 0;
    let new_investors_money = 0;

    for (let i = 1; i <= 10; i++) {
        people_per_day += people_per_day / 100 * annual_growth;
        earth_population += people_per_day
        gathered_money += paymentPerMonth;
        if (day !== 0){
            gathered_money -= investors_accounts * investor_percentage * paymentPerMonth;
        }
    
        // organizer_account += organizer_percentage * gathered_money;
        // gathered_money -= organizer_percentage * gathered_money;
        investors_count += 1;
        investors_accounts += 1;
        day++;
        pyramidResult.push({
            day,
            gathered_money,
            investors_accounts,
            investors_count,
            organizer_account
        })
    }
    
    while (gathered_money > 0 && organizer_max >= organizer_account ) {
        people_per_day += people_per_day * annual_growth;
        earth_population += people_per_day;
        day++;
        if (investors_count <= percentage_of_employees * earth_population && investors_count < investor_max){
            new_wave_investors_count = monthly_increase * investors_accounts;
            // new_wave_investors_count = percentage_of_employees * people_per_day;
        }
        else{
            new_wave_investors_count = percentage_of_employees * people_per_day;
        }
        if (day % 30 === 0){
            previous_investors_count = investors_count;
            if (day / 30 > 1){
                investors_count += new_wave_investors_count;
                new_investors_money = paymentPerMonth * new_wave_investors_count;
                gathered_money += new_investors_money + previous_investors_count * paymentPerMonth;
                if (organizer_max >= organizer_account){
                    organizer_account += organizer_percentage * gathered_money;
                    gathered_money -= organizer_percentage * gathered_money;
                }
                gathered_money -= investors_accounts * investor_percentage * paymentPerMonth;
                investors_accounts += new_wave_investors_count //+ previous_investors_count;
            }
            else{
                investors_count += new_wave_investors_count;
                new_investors_money = paymentPerMonth * new_wave_investors_count;
                gathered_money += new_investors_money;
                if (organizer_max >= organizer_account){
                    organizer_account += organizer_percentage * gathered_money;
                    gathered_money -= organizer_percentage * gathered_money;
                }
                gathered_money -= investors_accounts * investor_percentage * paymentPerMonth;
                investors_accounts += new_wave_investors_count
            }
        }
        else{
            investors_count += new_wave_investors_count;
            new_investors_money = paymentPerMonth * new_wave_investors_count;
            gathered_money += new_investors_money;
            if (organizer_max >= organizer_account){
                organizer_account += organizer_percentage * gathered_money;
                gathered_money -= organizer_percentage * gathered_money;
            }
            gathered_money -= investors_accounts * investor_percentage * paymentPerMonth;
            investors_accounts += new_wave_investors_count;
        }
        pyramidResult.push({
            day,
            gathered_money,
            investors_accounts,
            investors_count,
            organizer_account
        })
    }
    return pyramidResult;
}