export const Aw = (trialsSample: number):number => {
    const AwVach = 1.96 / Math.sqrt(trialsSample);
    return +AwVach.toFixed(4);
}

export const A = (lab: number, trialsSample: number):number => {
    return +(1.96 * Math.sqrt( (trialsSample * (4 - 1) + 1) / (4 * lab * trialsSample) )).toFixed(4);
}

export const Ar = (lab: number, trialsSample: number):number => {
    return +(1.96 * Math.sqrt( 1 / (2 * lab * (trialsSample - 1)) )).toFixed(4);
}

export const A_R = (lab: number, trialsSample: number):number => {
    const chis = lab * (1 + trialsSample * (4 - 1)) ^ 2 + (trialsSample - 1) * (lab - 1);
    const znam = 2 * 2 ^ 4 * trialsSample ^ 2 * (lab - 1) * lab
    return +(1.96 * Math.sqrt( chis / znam) ).toFixed(4);
}

export const numberSameValues = (values: number[]):number => {
    if (!values) return 0;
    let schet = 0,
        summ=0,
        mediana=0;
    mediana = median(values);
    values.forEach( v => v === mediana && schet++);
    summ = schet * (100 / values.length);
    if (summ > 50) {
        return average(values)
    }
    else{
        return mediana;
    }
}

function median(values: number[]){
    values.sort((a, b) => a - b);
    return (values[(values.length - 1) >> 1] + values[values.length >> 1]) / 2
}

function average(values: number[]) {
    return values.reduce( ( p, c ) => p + c, 0 ) / values.length
}