import { Organization } from './organization';

export interface AuthLocalLogin {
    identifier: string,
    password: string,
}

export interface UserLoginResponse {
    jwt: string,
    user: UserInfo
}

export interface UserInfo {
    id: number
    username: string
    email: string
    provider: string
    confirmed: boolean
    blocked: boolean
    role: Role
    created_at: string
    updated_at: string
    organization?: Organization
}

export interface Role {
    id: number,
    name: string,
    description: string,
    type: Roles
}

export enum Roles {
    Coordinate= "coordinate",
    Laborant= "laborant"
}