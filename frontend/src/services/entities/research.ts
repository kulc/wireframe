import { Organization } from './organization';
import { TypesOfTests } from './typesOfTests';

export interface Research {
    id: number,
    Title: string,
    dateEnd: string,
    numberOfTests: number,
    numberOfLaboratory: number,
    numberOfSamples: number,
    created_at: string,
    updated_at: string,
    ownerOrg: Organization,
    participantOrgs: Organization[],
    samples: ResearchSample[],
    types_of_tests: TypesOfTests[]
}

export interface ResearchSample {
    id: number,
    name: string,
    created_at: string,
    updated_at: string,
    organization: number
    research: number
}