import { TypesOfTests } from './typesOfTests'
import { Samples } from './samples'
import { Research } from './research'
import { Organization } from './organization'

export interface ResearchResult {
    id:	number
    measurementResult: number
    testNumber: number
    types_of_test: TypesOfTests
    sample: Samples
    research: Research
    organization: Organization
}

export interface NewResearchResult{
    measurementResult: number
    testNumber: number
    types_of_test: number
    sample: number
    research: number
    organization: number
}

export interface PutResearchResult{
    id: number
    measurementResult: number
    testNumber: number
    types_of_test: number
    sample: number
    research: number
    organization: number
}

export interface GetResultsRequest {
    testNumber?: number
    types_of_test?: number
    sample?: number
    research: number
    organization?: number
}