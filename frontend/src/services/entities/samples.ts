import { Organization } from './organization';

export interface Samples {
    id: number,
    name: string,
    organization: Organization
    created_at: string,
    updated_at: string,
}