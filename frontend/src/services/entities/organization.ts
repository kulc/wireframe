export interface Organization {
    id: number,
    Name: string,
    Type: OrganizationTypes,
    created_at: string,
    updated_at: string
}

export enum OrganizationTypes {
    Laboratory= "Laboratory",
    Center="Center"
}

export interface GetOrganizationsRequest {
    Type?: string
}