export const HTTP_STATUS = {
    CODE_401: 401,
    CODE_302: 302,
    CODE_404: 404,
    CODE_500: 500,
    CODE_200: 200,
}

export const USER_TOKEN_NAME = "USER_TOKEN";