import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { PageLayout } from './components/PageLayout';
import ru from 'antd/lib/locale-provider/ru_RU';
import { UserContext } from './services/contexts/userContext';
import { UserInfo } from './services/entities/user';
import { ConfigProvider } from 'antd/es';
import { checkAuth } from './services/api/login';
import { USER_TOKEN_NAME } from './constants/common';

class App extends Component {
  state = {
    user: undefined,
  }

  setUser = (user?: UserInfo) => {
    if (user)
        this.setState({user})
  }
  unsetUser = () => {
      this.setState({user: undefined});
  }

  setUserInfo = async () => {
    try {
      this.setUser(await checkAuth());
    } catch (error) {
      localStorage.removeItem(USER_TOKEN_NAME);
    }
    
  }

  componentDidMount(){
    this.setUserInfo();
  }

  render() {

    return (
      <BrowserRouter>
        <ConfigProvider locale={ru}>
          <UserContext.Provider value={{login: this.setUser, logout: this.unsetUser, user: this.state.user}}>
            <PageLayout></PageLayout>
          </UserContext.Provider>
        </ConfigProvider>
      </BrowserRouter>
    );
  }
}

export default App;