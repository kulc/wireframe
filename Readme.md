# Run
## Backend

```
cd ./backend
npm run develop
```

## Frontend

```
cd ./frontend
npm start
```

# Install
## Create database
Create postgres db:

```
name: strapi
log: strapi
pas: strapi
```

More: https://strapi.io/documentation/3.0.0-beta.x/installation/cli.html

## Install backend
```
cd ./backend
npm i
npm run develop
```

## Create data set
1. Open admin UI: http://localhost:1337/admin
2. Create Content-Type `Page` with attributes `id`, `Name:string`
3. Create new `Page`

## Install frontend
```
cd ./frontend
npm i
```
